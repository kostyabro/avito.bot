<?php

function getUrl($inputSearchPostArray) {
    if (is_array($inputSearchPostArray)) {
        $inputSearchPostString = http_build_query($inputSearchPostArray);
    } else {
        $inputSearchPostString = $inputSearchPostArray;
    }

    $userAgent      = 'Mozilla/5.0 (X11; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0';
    $cURL = curl_init();

    curl_setopt($cURL, CURLOPT_URL,             "https://www.avito.ru/search");
    curl_setopt($cURL, CURLOPT_HEADER,          TRUE);
    curl_setopt($cURL, CURLOPT_USERAGENT,       $userAgent);
    curl_setopt($cURL, CURLOPT_TIMEOUT,         10);
    curl_setopt($cURL, CURLOPT_RETURNTRANSFER,  TRUE);
    curl_setopt($cURL, CURLOPT_POST,            TRUE);
    curl_setopt($cURL, CURLOPT_POSTFIELDS,      $inputSearchPostString);
    curl_setopt($cURL, CURLOPT_AUTOREFERER,     TRUE);

    $answerFromServer = curl_exec($cURL);

    if (curl_getinfo($cURL, CURLINFO_HTTP_CODE) != 301) {
        return FALSE;
    }
    $outputUrl = substr($answerFromServer, stripos($answerFromServer, "Location: ") + 10);
    $outputUrl = substr($outputUrl, 0, strpos($outputUrl, "X-Frame-Options:") - 2);

    curl_close($cURL);

    return 'www.avito.ru'.$outputUrl;
}

$post = $_POST;

$link = getUrl($post);

$userAgent      = 'Mozilla/5.0 (X11; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0';
$cURL = curl_init();

curl_setopt($cURL, CURLOPT_URL,             $link);
curl_setopt($cURL, CURLOPT_HEADER,          TRUE);
curl_setopt($cURL, CURLOPT_USERAGENT,       $userAgent);
curl_setopt($cURL, CURLOPT_TIMEOUT,         10);
curl_setopt($cURL, CURLOPT_RETURNTRANSFER,  TRUE);
// curl_setopt($cURL, CURLOPT_POST,            TRUE);
// curl_setopt($cURL, CURLOPT_POSTFIELDS,      $inputSearchPostString);
curl_setopt($cURL, CURLOPT_AUTOREFERER,     TRUE);
curl_setopt($cURL, CURLOPT_FOLLOWLOCATION,  TRUE);

$answerFromServer = curl_exec($cURL);

if (curl_getinfo($cURL, CURLINFO_HTTP_CODE) == 404) {
    echo "<strong style=\"color: green\">По ссылке ".$link." ничего не найденно</strong><br /><hr />";
} else {
    echo "<strong style=\"color: green\">Запрашиваемая ссылка ".$link."</strong><br /><hr />";
    echo "
    <form action=\"createTask.php\" method=\"post\">

        <p>
            <input type=\"text\" name=\"searchlink\" value=\"".$link."\" hidden=\"true\" />
        </p>

        <p>
            <p><strong>Введите текст для отправки сообщения</strong>:</p>
            <textarea name=\"message\" hint=\"Сообщение\"></textarea>
        </p>
        
        <p>
            <p><strong>Ссылка которую надо отправить</strong>:</p>
            <textarea name=\"sendlink\" hint=\"Сообщение\"></textarea>
        </p>

        <p>
            <button type=\"submit\" name=\"createtask\">Запустить
            </button>
        </p>

    </form>
    <p><strong>После нажатия кнопки запустить вас перебросит на парсер. <br/>На всяки случай нажмите \"обновить страницу\". <br/>Он сделает вид, что быстро загрузился и покажет белый экран. <br/>Это нормально, он начал работать.</strong></p>
    ";
}

 ?>
