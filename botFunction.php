<?php
    error_reporting( E_ALL );

class Bot {

    public $sessionCoockie_sessid = "";
    public $sessionCoockie_u = "";
    public $sessionCoockie_v = "";
    public $sessionCoockie_anid = "";
    public $sessionCoockie_auth = "";

    // $userAgent      = 'User-Agent: Mozilla/4.0 (compatible; MSIE 5.01; Widows NT)';
    private $userAgent      = 'Mozilla/5.0 (X11; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0';
    private $cURL           = "";

    // TODO: creator coocke, rewrite var

    // TODO: 7-16 value
    function __construct() {
        $this->userAgent      = 'Mozilla/5.0 (X11; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0';
        $this->cURL           =  curl_init();
    }

    public function auth($email, $password) {
        $urlAvitoLogin  = 'https://www.avito.ru/profile/login';

        curl_setopt($this->cURL, CURLOPT_URL,             $urlAvitoLogin);
        curl_setopt($this->cURL, CURLOPT_HEADER,          TRUE);
        curl_setopt($this->cURL, CURLOPT_USERAGENT,       $this->userAgent);
        curl_setopt($this->cURL, CURLOPT_TIMEOUT,         10);
        curl_setopt($this->cURL, CURLOPT_RETURNTRANSFER,  TRUE);

        $answerFromServer = curl_exec($this->cURL);

        if (strpos($answerFromServer, 'sessid=')!== FALSE) $this->sessionCoockie_sessid  = substr($answerFromServer, strpos($answerFromServer, 'sessid=') + 7, 43);
        if (strpos($answerFromServer, 'u=')     !== FALSE) $this->sessionCoockie_u       = substr($answerFromServer, strpos($answerFromServer, 'u=') + 2, 26);
        if (strpos($answerFromServer, 'v=')     !== FALSE) $this->sessionCoockie_v       = substr($answerFromServer, strpos($answerFromServer, 'v=') + 2, 10);

        $hiddenNext = substr($answerFromServer, strpos($answerFromServer, "name=\"next\" value=") + 19, 8);

        $postAuth = "next=$hiddenNext&login=".$email."&password=".$password."&quick_expire=\"on\"";

        curl_setopt($this->cURL, CURLOPT_URL,             $urlAvitoLogin);
        curl_setopt($this->cURL, CURLOPT_HEADER,          TRUE);
        curl_setopt($this->cURL, CURLOPT_USERAGENT,       $this->userAgent);
        curl_setopt($this->cURL, CURLOPT_TIMEOUT,         10);
        curl_setopt($this->cURL, CURLOPT_RETURNTRANSFER,  TRUE);
        curl_setopt($this->cURL, CURLOPT_COOKIE,          "sessid=".$this->sessionCoockie_sessid."; u=".$this->sessionCoockie_u."; v=".$this->sessionCoockie_v);
        curl_setopt($this->cURL, CURLOPT_POST,            TRUE);
        curl_setopt($this->cURL, CURLOPT_REFERER,         $urlAvitoLogin);
        curl_setopt($this->cURL, CURLOPT_POSTFIELDS,      $postAuth);

        $answerFromServer = curl_exec($this->cURL);

        if (strpos($answerFromServer, 'sessid=')!== FALSE) $this->sessionCoockie_sessid  = substr($answerFromServer, strpos($answerFromServer, 'sessid=') + 7, 43);
        if (strpos($answerFromServer, 'u=')     !== FALSE) $this->sessionCoockie_u       = substr($answerFromServer, strpos($answerFromServer, 'u=') + 2, 26);
        if (strpos($answerFromServer, 'v=')     !== FALSE) $this->sessionCoockie_v       = substr($answerFromServer, strpos($answerFromServer, 'v=') + 2, 10);
        if (strpos($answerFromServer, 'anid=')  !== FALSE) $this->sessionCoockie_anid    = substr($answerFromServer, strpos($answerFromServer, 'anid=') + 5, 36);
        if (strpos($answerFromServer, 'auth=')  !== FALSE) $this->sessionCoockie_auth    = substr($answerFromServer, strpos($answerFromServer, 'auth=') + 5, 1);

        if (0 === strnatcmp($this->sessionCoockie_auth, '1')) {
            return TRUE;
        } else {
            return FALSE;
        }

    }

    public function sendMessage($link, $itemId, $message) {

        $link = "https://www.avito.ru".$link;
        curl_setopt($this->cURL, CURLOPT_URL,              $link);
        curl_setopt($this->cURL, CURLOPT_HEADER,           TRUE);
        curl_setopt($this->cURL, CURLOPT_USERAGENT,        $this->userAgent);
        curl_setopt($this->cURL, CURLOPT_TIMEOUT,          10);
        curl_setopt($this->cURL, CURLOPT_RETURNTRANSFER,   TRUE);
        curl_setopt($this->cURL, CURLOPT_POST,             FALSE);
        curl_setopt($this->cURL, CURLOPT_COOKIE, "sessid=".$this->sessionCoockie_sessid.
        "; u=".$this->sessionCoockie_u."; v=".$this->sessionCoockie_v.
        "; anid=".$this->sessionCoockie_anid."; auth=".$this->sessionCoockie_auth);

        $answerFromServer = curl_exec($this->cURL);

        $hiddenTokenName        = substr($answerFromServer, strpos($answerFromServer, "name=\"token[") + 12, 13);
        $hiddenTokenValue       = substr($answerFromServer, strpos($answerFromServer, "name=\"token[$hiddenTokenName]\" value=\"") + 35, 16);
        $hiddenSearchHashValue  = substr($answerFromServer, strpos($answerFromServer, "name=\"searchHash\" value=\"") + 25, 31);

        $postMess = "token[$hiddenTokenName]=$hiddenTokenValue&comment=$message&searchHash=$hiddenSearchHashValue";

        curl_setopt($this->cURL, CURLOPT_URL,              "https://www.avito.ru/items/write/$itemId");
        curl_setopt($this->cURL, CURLOPT_HEADER,           TRUE);
        curl_setopt($this->cURL, CURLOPT_USERAGENT,        $this->userAgent);
        curl_setopt($this->cURL, CURLOPT_TIMEOUT,          10);
        curl_setopt($this->cURL, CURLOPT_RETURNTRANSFER,   TRUE);
        curl_setopt($this->cURL, CURLOPT_COOKIE,           "sessid=".$this->sessionCoockie_sessid.
        "; u=".$this->sessionCoockie_u."; v=".$this->sessionCoockie_v.
        "; anid=".$this->sessionCoockie_anid."; auth=".$this->sessionCoockie_auth);
        curl_setopt($this->cURL, CURLOPT_POST,             TRUE);
        curl_setopt($this->cURL, CURLOPT_REFERER,          $link);
        curl_setopt($this->cURL, CURLOPT_POSTFIELDS,       $postMess);

        $headerArray = array(
            "X-Requested-With" => "XMLHttpRequest",
        );

        curl_setopt($this->cURL, CURLOPT_HTTPHEADER,       $headerArray);

        $answerFromServer = curl_exec($this->cURL);

        return $answerFromServer;
    /*
        if (substr($answerFromServer, strpos($answerFromServer, "HTTP/1.1 ") + 9, 3) == "200") {
            return TRUE;
        } else {
            return FALSE;
        }*/
    }

    public function showSite(){
        $url = "https://www.avito.ru/ekaterinburg";

        curl_setopt($this->cURL, CURLOPT_URL,             $url);
        curl_setopt($this->cURL, CURLOPT_HEADER,          TRUE);
        curl_setopt($this->cURL, CURLOPT_USERAGENT,       $this->userAgent);
        curl_setopt($this->cURL, CURLOPT_TIMEOUT,         10);
        curl_setopt($this->cURL, CURLOPT_RETURNTRANSFER,  TRUE);
        curl_setopt($this->cURL, CURLOPT_COOKIE,           "sessid=".$this->sessionCoockie_sessid.
        "; u=".$this->sessionCoockie_u."; v=".$this->sessionCoockie_v.
        "; anid=".$this->sessionCoockie_anid."; auth=".$this->sessionCoockie_auth);
        curl_setopt($this->cURL, CURLOPT_POST,            FALSE);
        // curl_setopt($GLOBALS["cURL"], CURLOPT_REFERER,         $urlAvitoLogin);
        curl_setopt($this->cURL, CURLINFO_HEADER_OUT,     TRUE);

        $answerFromServer = curl_exec($this->cURL);

        $answerFromServer = str_replace("action=\"/search\"", "action=\"search.php\"", $answerFromServer);

        echo "$answerFromServer";

        return TRUE;
    }

    function __destruct() {
        curl_close($this->cURL);
    }

    // if (TRUE === auth()) {
    //     echo "<h1>Авторизованно</h1>";
    // } else {
    //     echo "<h1>Ошибка авторизации</h1>";
    // }

    // require "db.php";
    //
    // auth();
    // showSite();
    // sendMessage("https://www.avito.ru/ekaterinburg/koshki/kotik_1132615235", "1132615235", "Внимание... Предлагаю очень выгодный обмен. https://www.avito.ru/ekaterinburg/koshki/kotik_1132615235. Звоните, До");

    // curl_close($cURL);

}

 ?>
