<?php

    error_reporting( E_ALL );

    require 'constants.php';

    function createBD(){
        $host   = $GLOBALS["host"];
        $dbName = $GLOBALS["dbName"];

        $pdo = new PDO("mysql:host=$host;dbname=$dbName", $GLOBALS["userName"], $GLOBALS["password"]);

        if ( count(($pdo->query("show tables like 'avitoUser';"))->fetchAll()) == 0 ) {
            $pdo->query("CREATE TABLE `avitoUser` (
                `avitoUser_id` int(1) unsigned NOT NULL AUTO_INCREMENT,
                `login` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
                `pass` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
                PRIMARY KEY (`avitoUser_id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");
        }

        if ( count(($pdo->query("show tables like 'sites';"))->fetchAll()) == 0 ) {
            $pdo->query("CREATE TABLE `sites` (
                `sites_id` int(1) unsigned NOT NULL AUTO_INCREMENT,
                `link` varchar(512) COLLATE utf8_unicode_ci,
                `is_send` varchar(512) COLLATE utf8_unicode_ci,
                `is_button` varchar(512) COLLATE utf8_unicode_ci,
                PRIMARY KEY (`sites_id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;"); // TODO: create
        }
        return true;
    }

 ?>
