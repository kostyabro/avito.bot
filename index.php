<?php
    error_reporting( E_ALL );

    require "db.php";
    require "botFunction.php";

    $allGood = FALSE;

    $post = $_POST;
    if ( isset($post['enter']) ) {

        $errors = array();
        if ( $post['botPass'] == '' ) {
            $errors[] = 'Введите пароль от бота';
        }

        $bot_bd = null;
        $i = 0;
        while ($bot_bd == null) {
            $bot_bd = R::findOne("bot", "id = ?", array($i));
            $i++;
        }
        // print_r($);
        if (password_verify($post['botPass'], $bot_bd->pass)) {

        } else {
            $errors[] = 'Пароль от бота неправильный';
        }

        if ( trim($post['avitoEmail']) == '' ) {
            $errors[] = 'Введите email от avito';
        }

        if ( $post['avitoPass'] == '' ) {
            $errors[] = 'Введите пароль от avito';
        }

    }

    if ( isset($post['createPassword']) ) {

        $errors = array();
        if ( $post['createBotPass'] == '' ) {
            $errors[] = 'Введите пароль от бота';
        }

    }
    
    if ( isset($post['deleteuser']) ) {
        R::wipe('user');
        echo '<div style="color: green">Данные о пользователе авито удалены.
        </div><hr />';
    }

    // echo empty($errors) === false;

    if ( empty($errors) ) {
        if ( isset($post['enter']) ) {
            $bot = new Bot();
            $isAuth = $bot->auth($post['avitoEmail'], $post['avitoPass']);
            if ($isAuth) {
                $table_user         = R::dispense('user');
                $table_user->email  = $post['avitoEmail'];
                $table_user->pass   = $post['avitoPass'];
                $table_user->sessid = $bot->sessionCoockie_sessid;
                $table_user->u      = $bot->sessionCoockie_u;
                $table_user->v      = $bot->sessionCoockie_v;
                $table_user->anid   = $bot->sessionCoockie_anid;
                $table_user->auth   = $bot->sessionCoockie_auth;
                R::store($table_user);
                $allGood = TRUE;
                echo '<div style="color: green">Авторизация прошла успешно.
                </div><hr />';
            } else {
                echo '<div style="color: red">Неверный логин или пароль от авито или авито начал запрашивать капчу для вашего аккаунта.<br />
                Т.к. бот не сможет разпознать капчу без помощи человека, то он и не сможет отправлять чообщения.<br />
                Вы можете подождать пока авито перестанет запрашивать на вас капчу, или же создать новый аккаунт.
                </div><hr />';
            }
        }

        if ( isset($post['createPassword']) ) {
            $table_bot = R::dispense('bot');
            $table_bot->pass = password_hash($post['createBotPass'], PASSWORD_DEFAULT);
            R::store($table_bot);
            echo '<div style="color: green">Пароль сохранен.
            </div><hr />';
        }
    } else {
        echo '<div style="color: red">'.array_shift($errors).
        '</div><hr />';
    }

 // $listOfTables = R::inspect();
 //
 // print_r($listOfTables);

 ?>

<!-- Вывод форм -->
<?php

    $formCreatePassword = "
    <form action=\"index.php\" method=\"post\">

        <p>
            <p><strong>Пароль для бота не установлен<br />Введите пароль</strong>:</p>
            <input type=\"password\" name=\"createBotPass\" value=\"".@$post['createBotPass']."\" />
        </p>

        <p>
            <button type=\"submit\" name=\"createPassword\">Задать пароль
            </button>
        </p>

    </form>
    ";

    $formEnter = "
    <form action=\"index.php\" method=\"post\">

        <p>
            <p><strong>Пароль от Бота</strong>:</p>
            <input type=\"password\" name=\"botPass\" value=\"".@$post['botPass']."\"/>
        </p>

        <p>
            <p><strong>Avito e-mail</strong>:</p>
            <input type=\"email\" name=\"avitoEmail\" value=\"".@$post['avitoEmail']."\"/>
       </p>

        <p>
            <p><strong>Avito пароль</strong>:</p>
            <input type=\"password\" name=\"avitoPass\"  value=\"".@$post['avitoPass']."\"/>
        </p>

        <p>
            <button type=\"submit\" name=\"enter\">Войти
            </button>
        </p>

    </form>
    
    <form action=\"index.php\" method=\"post\">

        <p>
            <button type=\"submit\" name=\"deleteuser\">Удалить данные о пользователе авито
            </button>
        </p>

    </form>
    
    ";

    if ($allGood) {

        if (is_null($bot)) $bot = new Bot();
        $bot->showSite();

    } else {
        if (in_array('bot', R::inspect())) {
            if (R::count('bot') < 1) {
                echo $formCreatePassword;
            } else {
                echo $formEnter;
            }
        } else {
            echo $formCreatePassword;
        }
    }

	$select_for_all = R::getAll('SELECT * FROM `listofproduct`');
//	print_r($select_for_count);
        $count = count($select_for_all) - 2;
        echo 'всего       : '.$count;

	$select_for_true = R::getAll('SELECT * FROM `listofproduct` WHERE `issend` = \'true\'');
	$count = count($select_for_true);
	echo '<br>отправленно : '.$count;
?>
